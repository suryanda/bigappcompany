#1
To Start the application, Run the command :'npm install' then 'npm run start'

#2
Go to "localhost:3000/register" make a post request with body in JSON format like this 
{
	"email":".....",
	"password": "",
	"DOB":".....",
	"username":"......",
	"role":"......."
} 
provide the role of a user, for admin provide admin.

#3
Once registered successfully, logIn . To login go to localhost:3000/login
and post data like this 
{
	"email":"....",
	"password": "..."
}
in response you will get a token. 

#4
To view all the users go to localhost:3000/users, make a get request and pass in header as Admin : Token received after logged in

#5
To check parenthesis is balanced or not go to localhost:3000/balanced/"objectID of a user", make a post request with body in JSON format and pass in header as Authorisation : Token received after logged in.

#6
To delete a user go to localhost:3000/user/delete/"object id of user you want to delete", make a get request and pass in header as Admin : Token received after logged in
